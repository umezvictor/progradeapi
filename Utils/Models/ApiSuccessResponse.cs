﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Utils.Models
{
    public class ApiSuccessResponse
    {
        public string Message { get; set; }
       
        public int StatusCode { get; set; }
    }
}
