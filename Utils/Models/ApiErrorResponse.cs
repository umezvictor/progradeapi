﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Utils.Models
{
    public class ApiErrorResponse
    {
        public string Message { get; set; }
        public string Details { get; set; }
        public bool IsSuccessful { get; set; } = false; //default value
        public int StatusCode { get; set; }
    }
}
