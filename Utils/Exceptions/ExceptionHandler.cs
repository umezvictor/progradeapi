﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Utils.Exceptions
{
    public class ExceptionHandler : Exception
    {
        public ExceptionHandler(string message) : base(message)
        {

        }
    }
}
