﻿using BLL.Request;
using DLL.Model;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BLL.Services
{
    public interface IStudentService
    {
        Task<Student> DeleteAsync(string email);
        // Task<List<Student>> GetAllAsync();
        IQueryable<Student> GetAllAsync();
        Task<Student> GetSingleAsync(string email);
        Task<Student> InsertAsync(StudentInsertRequestViewModel request);
        Task<Student> UpdateAsync(string email, Student Student);

        Task<bool> IsEmailExist(string email);
        Task<bool> IsIdExist(int studentId);
        Task<bool> IsDepartmentExist(int id);
    }



}
