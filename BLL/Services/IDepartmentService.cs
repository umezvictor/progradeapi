﻿using BLL.Request;
using DLL.Model;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BLL.Services
{
    public interface IDepartmentService
    {
        Task<Department> DeleteAsync(string code);
        /*Task<List<Department>> GetAllAsync();*/
        IQueryable<Department> GetAllAsync();
        Task<Department> GetSingleAsync(string code);
        Task<Department> InsertAsync(DepartmentInsertRequestViewModel request);
        Task<Department> UpdateAsync(string code, Department department);
        Task<bool> IsCodeExist(string code);
        Task<bool> IsNameExist(string name);
        Task<bool> IsIdExist(int id);
    }



}
