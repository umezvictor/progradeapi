﻿using BLL.Request;
using DLL.Model;
using DLL.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utils.Exceptions;

namespace BLL.Services
{
    public class DepartmentService : IDepartmentService
    {
       
        private readonly IUnitOfWork _unitOfWork;

        public DepartmentService(IUnitOfWork unitOfWork)
        {
           
            _unitOfWork = unitOfWork;
        }

        public async Task<Department> DeleteAsync(string code)
        {
            var department = await _unitOfWork.DepartmentRepository.FindSingleAsync(x => x.Code == code);
            if(department == null)
            {
                throw new ExceptionHandler("Department not found");
            }

            _unitOfWork.DepartmentRepository.Delete(department);
            if (await _unitOfWork.SaveChangesAsync())
            {
                return department;
            }

            throw new ExceptionHandler("Error deleting department");
        }

       /* public async Task<List<Department>> GetAllAsync()
        {
            return await _unitOfWork.DepartmentRepository.GetList();
        }*/

        public  IQueryable<Department> GetAllAsync()
        {
            return _unitOfWork.DepartmentRepository.QueryAll();
        }

        public async Task<Department> GetSingleAsync(string code)
        {
            var department = await _unitOfWork.DepartmentRepository.FindSingleAsync(x => x.Code == code);
            if(department == null)
            {
                throw new ExceptionHandler("department not found");

            }
            return department;
        }

        public async Task<Department> InsertAsync(DepartmentInsertRequestViewModel request)
        {
            Department department = new Department();
            department.Name = request.Name;
            department.Code = request.Code;

            await _unitOfWork.DepartmentRepository.CreateAsync(department);
            if(await _unitOfWork.SaveChangesAsync())
            {
                return department;
            }
            throw new ExceptionHandler("Failed to insert department");
        }


        public async Task<bool> IsCodeExist(string code)
        {
            var department = await _unitOfWork.DepartmentRepository.FindSingleAsync(x => x.Code == code);
            if(department == null)
            {
                return true;
            }
            return false;
        }

        public async Task<bool> IsIdExist(int id)
        {
            var department = await _unitOfWork.DepartmentRepository.FindSingleAsync(x => x.DepartmentId == id);
            if (department == null)
            {
                return true;
            }
            return false;
        }

        public async Task<bool> IsNameExist(string name)
        {
            var department = await _unitOfWork.DepartmentRepository.FindSingleAsync( x => x.Name == name);
            if (department == null)
            {
                return true;
            }
            return false;
        }

        public async Task<Department> UpdateAsync(string code, Department adepartment)
        {
            var department = await _unitOfWork.DepartmentRepository.FindSingleAsync(x => x.Code == code);
            if (department == null)
            {
                throw new ExceptionHandler("Department not found");
            }

            if (!string.IsNullOrWhiteSpace(adepartment.Code))
            {
                var existingCode = await _unitOfWork.DepartmentRepository.FindSingleAsync(x => x.Code == code);
                if (existingCode != null)
                {
                    throw new ExceptionHandler("Code already exists");

                }
                department.Code = adepartment.Code;
            }

            if (!string.IsNullOrWhiteSpace(adepartment.Name))
            {
                var existingCode = await _unitOfWork.DepartmentRepository.FindSingleAsync(x => x.Name == adepartment.Name);
                if (existingCode != null)
                {
                    throw new ExceptionHandler("Name already exists");

                }
                department.Name = adepartment.Name;
            }
            _unitOfWork.DepartmentRepository.Update(adepartment);
            if (await _unitOfWork.SaveChangesAsync())
            {
                return department;
            }

            throw new ExceptionHandler("Update failed");

        }
    }
}
