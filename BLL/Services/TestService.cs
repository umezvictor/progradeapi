﻿using BLL.Request;
using Bogus;
using DLL.DBContext;
using DLL.Model;
using DLL.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utils.Exceptions;

namespace BLL.Services
{
    public class TestService : ITestService
    {
       
        private readonly IUnitOfWork _unitOfWork;
        private readonly AppDbContext _context;

        public TestService(IUnitOfWork unitOfWork, AppDbContext context)
        {
            
            _unitOfWork = unitOfWork;
            _context = context;
        }

        public async Task InsertDummyData()
        {


            var dummyStudents = new Faker<Student>()
    
            .RuleFor(u => u.Name, (f, u) => f.Name.FullName())
            .RuleFor(u => u.Email, (f, u) => f.Internet.Email(u.Name));
            var dummyDepartments = new Faker<Department>()           
            .RuleFor(o => o.Name, f => f.Name.FirstName())
           
            .RuleFor(o => o.Code, f => f.Name.LastName())

            //insert dummy data into students and departments tables simultaneously
            .RuleFor(u => u.Students, f => dummyStudents.Generate(50).ToList());

            var departmentListWithstudent = dummyDepartments.Generate(100).ToList();

            await _context.Departments.AddRangeAsync(departmentListWithstudent);
            await _context.SaveChangesAsync();
            }

        //inserts dummy data into courses and coursestudents table
        public async Task InsertDummyData2()
        {
            var dummyCourses = new Faker<Course>()

           .RuleFor(u => u.Name, (f, u) => f.Name.FullName())
           .RuleFor(u => u.Code, (f, u) => f.Name.LastName())
           .RuleFor(u => u.Credit, (f, u) => f.Random.Number(1, 10));
            var courseList = dummyCourses.Generate(50).ToList();
            await _context.Courses.AddRangeAsync(courseList);
            await _context.SaveChangesAsync();

            //get all stdent and course ids
            var studentIds = await _context.Students.Select(x => x.StudentId).ToListAsync();
            var coursetIds = await _context.Courses.Select(x => x.CourseId).ToListAsync();

            int count = 0;

            //enroll studens in courses
            foreach(var course in coursetIds)
            {
                var courseStudent = new List<CourseStudent>();
                var students = studentIds.Skip(count).Take(5).ToList();

                foreach( var astudent in students)
                {
                    courseStudent.Add(new CourseStudent()
                    {
                        CourseId = course,
                        StudentId = astudent
                    });
                }

                await _context.CourseStudents.AddRangeAsync(courseStudent);
                await _context.SaveChangesAsync();
                count += 5;
            }
            
            
        }
    }
}
