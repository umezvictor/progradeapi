﻿using BLL.Request;
using DLL.Model;
using DLL.Repositories.Interfaces;
using DLL.ResponseViewModel;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Utils.Exceptions;
using Utils.Models;

namespace BLL.Services
{
    public class CourseStudentService : ICourseStudentService
    {
       
        private readonly IUnitOfWork _unitOfWork;

        public CourseStudentService(IUnitOfWork unitOfWork)
        {
           
            _unitOfWork = unitOfWork;
        }

        public async Task<StudentCourseViewModel> CourseListAsync(int studentId)
        {
            return await _unitOfWork.StudentRepository.GetSpecificStudentCourseListAsync(studentId);
        }

        public async Task<CourseStudent> DeleteAsync(int courseId)
        {
            var courseStudent = await _unitOfWork.CourseStudentRepository.FindSingleAsync(x => x.CourseId == courseId);
            if(courseStudent == null)
            {
                throw new ExceptionHandler("Record not found");
            }

            _unitOfWork.CourseStudentRepository.Delete(courseStudent);
            if (await _unitOfWork.SaveChangesAsync())
            {
                return courseStudent;
            }

            throw new ExceptionHandler("Error deleting record");
        }

        public async Task<List<CourseStudent>> GetAllAsync()
        {
            return await _unitOfWork.CourseStudentRepository.GetList();
        }

        public async Task<CourseStudent> GetSingleAsync(int courseId)
        {
            var courseStudent = await _unitOfWork.CourseStudentRepository.FindSingleAsync(x => x.CourseId == courseId);
            if(courseStudent == null)
            {
                throw new ExceptionHandler("Course not found");

            }
            return courseStudent;
        }

        public async Task<ApiSuccessResponse> InsertAsync(CourseAssignInsertViewModel request)
        {
            var studentEnrolled = await _unitOfWork.CourseStudentRepository.FindSingleAsync(
                x => x.CourseId == request.CourseId && x.StudentId == request.StudentId);

            //check if student has already been enrolled for a course
            if(studentEnrolled != null)
            {
                throw new ExceptionHandler("Student already enrolled for this course");
            }

            var courseStudent = new CourseStudent()
            {
                StudentId = request.StudentId,
                CourseId = request.CourseId
            };

            await _unitOfWork.CourseStudentRepository.CreateAsync(courseStudent);
            if(await _unitOfWork.SaveChangesAsync())
            {
                return new ApiSuccessResponse()
                {
                    StatusCode = 200,
                    Message = "Student enrolled successfully"
                };
            }

            throw new ExceptionHandler("Student enrollment failes, please try again");
        }


        public async Task<bool> IsCodeExist(string code)
        {
            var course = await _unitOfWork.CourseRepository.FindSingleAsync(x => x.Code == code);
            if(course == null)
            {
                return true;
            }
            return false;
        }

      

        public async Task<bool> IsNameExist(string name)
        {
            var course = await _unitOfWork.CourseRepository.FindSingleAsync( x => x.Name == name);
            if (course == null)
            {
                return true;
            }
            return false;
        }

       /* public async Task<CourseStudent> UpdateAsync(int code, CourseStudent acourse)
        {
            var course = await _unitOfWork.CourseRepository.FindSingleAsync(x => x.Code == code);
            if (course == null)
            {
                throw new ExceptionHandler("Course not found");
            }

            if (!string.IsNullOrWhiteSpace(acourse.Code))
            {
                var existingCode = await _unitOfWork.CourseRepository.FindSingleAsync(x => x.Code == code);
                if (existingCode != null)
                {
                    throw new ExceptionHandler("Code already exists");

                }
                course.Code = acourse.Code;
            }

            if (!string.IsNullOrWhiteSpace(acourse.Name))
            {
                var existingCode = await _unitOfWork.CourseRepository.FindSingleAsync(x => x.Name == acourse.Name);
                if (existingCode != null)
                {
                    throw new ExceptionHandler("Name already exists");

                }
                course.Name = acourse.Name;
            }
            _unitOfWork.CourseRepository.Update(acourse);
            if (await _unitOfWork.SaveChangesAsync())
            {
                return course;
            }

            throw new ExceptionHandler("Update failed");

        }*/
    }
}
