﻿using BLL.Request;
using DLL.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Services
{
    //this class is just for inserting dummy data -- seeding
    public interface ITestService
    {
        Task InsertDummyData();
        Task InsertDummyData2();
       
    }



}
