﻿using BLL.Request;
using DLL.Model;
using DLL.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Utils.Exceptions;

namespace BLL.Services
{
    public class CourseService : ICourseService
    {
       
        private readonly IUnitOfWork _unitOfWork;

        public CourseService(IUnitOfWork unitOfWork)
        {
           
            _unitOfWork = unitOfWork;
        }

        public async Task<Course> DeleteAsync(string code)
        {
            var Course = await _unitOfWork.CourseRepository.FindSingleAsync(x => x.Code == code);
            if(Course == null)
            {
                throw new ExceptionHandler("Course not found");
            }

            _unitOfWork.CourseRepository.Delete(Course);
            if (await _unitOfWork.SaveChangesAsync())
            {
                return Course;
            }

            throw new ExceptionHandler("Error deleting Course");
        }

        public async Task<List<Course>> GetAllAsync()
        {
            return await _unitOfWork.CourseRepository.GetList();
        }

        public async Task<Course> GetSingleAsync(string code)
        {
            var Course = await _unitOfWork.CourseRepository.FindSingleAsync(x => x.Code == code);
            if(Course == null)
            {
                throw new ExceptionHandler("Course not found");

            }
            return Course;
        }

        public async Task<Course> InsertAsync(CourseInsertRequestViewModel request)
        {
            Course course = new Course();
            course.Name = request.Name;
            course.Code = request.Code;
            course.Credit = request.Credit;

            await _unitOfWork.CourseRepository.CreateAsync(course);
            if(await _unitOfWork.SaveChangesAsync())
            {
                return course;
            }
            throw new ExceptionHandler("Failed to insert Course");
        }


        public async Task<bool> IsCodeExist(string code)
        {
            var course = await _unitOfWork.CourseRepository.FindSingleAsync(x => x.Code == code);
            if(course == null)
            {
                return true;
            }
            return false;
        }

      

        public async Task<bool> IsNameExist(string name)
        {
            var course = await _unitOfWork.CourseRepository.FindSingleAsync( x => x.Name == name);
            if (course == null)
            {
                return true;
            }
            return false;
        }

        public async Task<bool> IsIdExist(int courseId)
        {
            var course = await _unitOfWork.CourseRepository.FindSingleAsync(x => x.CourseId == courseId);
            if (course == null)
            {
                return true;
            }
            return false;
        }

        public async Task<Course> UpdateAsync(string code, Course acourse)
        {
            var course = await _unitOfWork.CourseRepository.FindSingleAsync(x => x.Code == code);
            if (course == null)
            {
                throw new ExceptionHandler("Course not found");
            }

            if (!string.IsNullOrWhiteSpace(acourse.Code))
            {
                var existingCode = await _unitOfWork.CourseRepository.FindSingleAsync(x => x.Code == code);
                if (existingCode != null)
                {
                    throw new ExceptionHandler("Code already exists");

                }
                course.Code = acourse.Code;
            }

            if (!string.IsNullOrWhiteSpace(acourse.Name))
            {
                var existingCode = await _unitOfWork.CourseRepository.FindSingleAsync(x => x.Name == acourse.Name);
                if (existingCode != null)
                {
                    throw new ExceptionHandler("Name already exists");

                }
                course.Name = acourse.Name;
            }
            _unitOfWork.CourseRepository.Update(acourse);
            if (await _unitOfWork.SaveChangesAsync())
            {
                return course;
            }

            throw new ExceptionHandler("Update failed");

        }
    }
}
