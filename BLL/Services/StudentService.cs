﻿using BLL.Request;
using DLL.Model;
using DLL.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utils.Exceptions;

namespace BLL.Services
{
    public class StudentService : IStudentService
    {
       
        private readonly IUnitOfWork _unitOfWork;

        public StudentService(IUnitOfWork unitOfWork)
        {
            
            _unitOfWork = unitOfWork;
        }

        public async Task<Student> DeleteAsync(string email)
        {
            var student = await _unitOfWork.StudentRepository.FindSingleAsync(x => x.Email == email);
            if (student == null)
            {
                throw new ExceptionHandler("Student not found");
            }

            _unitOfWork.StudentRepository.Delete(student);
            if (await _unitOfWork.SaveChangesAsync())
            {
                return student;
            }

            throw new ExceptionHandler("Error deleting Student");
        }

        public IQueryable<Student> GetAllAsync()
        {
            return _unitOfWork.StudentRepository.QueryAll();
        }

        public async Task<Student> GetSingleAsync(string email)
        {
            var student = await _unitOfWork.StudentRepository.FindSingleAsync(x => x.Email == email);
            if (student == null)
            {
                throw new ExceptionHandler("Student not found");

            }
            return student;
        }

        public async Task<Student> InsertAsync(StudentInsertRequestViewModel request)
        {
            Student student = new Student();
            student.Name = request.Name;
            student.Email = request.Email;
            student.DepartmentId = request.DepartmentId;

            await _unitOfWork.StudentRepository.CreateAsync(student);
            if (await _unitOfWork.SaveChangesAsync())
            {
                return student;
            }
            throw new ExceptionHandler("Failed to insert Student");
        }


       

        public async Task<bool> IsDepartmentExist(int id)
        {
           
            var student = await _unitOfWork.StudentRepository.FindSingleAsync(x => x.DepartmentId == id);
            if (student == null)
            {
                return true;
            }
            return false;
        }

        public async Task<bool> IsEmailExist(string email)
        {
            var student = await _unitOfWork.StudentRepository.FindSingleAsync(x => x.Email == email);
            if (student == null)
            {
                return true;
            }
            return false;
        }

        public async Task<bool> IsIdExist(int studentId)
        {
            var student = await _unitOfWork.StudentRepository.FindSingleAsync(x => x.StudentId == studentId);
            if (student == null)
            {
                return true;
            }
            return false;
        }



        public async Task<Student> UpdateAsync(string email, Student model)
        {
            var student = await _unitOfWork.StudentRepository.FindSingleAsync(x => x.Email == email);
            if (student == null)
            {
                throw new ExceptionHandler("Student not found");
            }

            student.Name = model.Name;
            _unitOfWork.StudentRepository.Update(student);
            if (await _unitOfWork.SaveChangesAsync())
            {
                return student;
            }

            throw new ExceptionHandler("Update failed");

        }
    }
}
