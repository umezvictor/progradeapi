﻿using BLL.Request;
using DLL.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Services
{
    public interface ICourseService
    {
        Task<Course> DeleteAsync(string code);
        Task<List<Course>> GetAllAsync();
        Task<Course> GetSingleAsync(string code);
        Task<Course> InsertAsync(CourseInsertRequestViewModel request);
        Task<Course> UpdateAsync(string code, Course Course);
        Task<bool> IsCodeExist(string code);
        Task<bool> IsNameExist(string name);
        Task<bool> IsIdExist(int courseId);
       
    }



}
