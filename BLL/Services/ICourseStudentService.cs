﻿using BLL.Request;
using DLL.Model;
using DLL.ResponseViewModel;
using System.Collections.Generic;
using System.Threading.Tasks;
using Utils.Models;

namespace BLL.Services
{
    public interface ICourseStudentService
    {
        Task<CourseStudent> DeleteAsync(int courseId);
        Task<List<CourseStudent>> GetAllAsync();
        Task<CourseStudent> GetSingleAsync(int courseId);
        Task<ApiSuccessResponse> InsertAsync(CourseAssignInsertViewModel request);
       Task<StudentCourseViewModel> CourseListAsync(int studentId);
       
       
    }



}
