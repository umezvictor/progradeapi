﻿using BLL.Services;
using FluentValidation;
using Microsoft.EntityFrameworkCore.Metadata;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Extensions.DependencyInjection;

namespace BLL.Request
{
    
    public class DepartmentInsertRequestViewModel
    {
        
        public string Name { get; set; }
        public string Code { get; set; }
        public int Id { get; set; }
    }

    //we did not use the usual annotation for validation
    public class DepartmentInsertRequestViewModelValidator : AbstractValidator<DepartmentInsertRequestViewModel>
    {
        private readonly IServiceProvider _serviceProvider;

        public DepartmentInsertRequestViewModelValidator(IServiceProvider serviceProvider)
        {
            
            //validation rules
            RuleFor(x => x.Name).NotNull().NotEmpty().MinimumLength(4)
                .MaximumLength(25).MustAsync(NameExists).WithMessage("Name already exists");
            RuleFor(x => x.Code).NotNull().NotEmpty().MinimumLength(3)
                .MaximumLength(10).MustAsync(CodeExists).WithMessage("Code already exists");

            RuleFor(x => x.Id).GreaterThan(0).MustAsync(DepartmentExists).WithMessage("Id already exists");


            _serviceProvider = serviceProvider;
        }

        //here, you can reference your custom validation
        //IsCodeExists is in idepartmentservice implememntation
        private async Task<bool> CodeExists(string code, CancellationToken arg2)
        {
            if (string.IsNullOrEmpty(code))
            {
                return true;
            }
            var requiredService = _serviceProvider.GetRequiredService<IDepartmentService>();

            return await requiredService.IsCodeExist(code);
        }

        private async Task<bool> NameExists(string name, CancellationToken arg2)
        {
            if (string.IsNullOrEmpty(name))
            {
                return true;
            }
            var requiredService = _serviceProvider.GetRequiredService<IDepartmentService>();

            return await requiredService.IsNameExist(name);
        }

        private async Task<bool> DepartmentExists(int id, CancellationToken arg2)
        {

            var requiredService = _serviceProvider.GetRequiredService<IDepartmentService>();

            return !await requiredService.IsIdExist(id);
        }
    }
}
