﻿using BLL.Services;
using FluentValidation;
using Microsoft.EntityFrameworkCore.Metadata;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Extensions.DependencyInjection;

namespace BLL.Request
{
    
    public class CourseInsertRequestViewModel
    {
        
        public string Name { get; set; }
        public string Code { get; set; }
        public decimal Credit{ get; set; }
    }

    //we did not use the usual annotation for validation
    public class CourseInsertRequestViewModelValidator : AbstractValidator<CourseInsertRequestViewModel>
    {
        private readonly IServiceProvider _serviceProvider;

        public CourseInsertRequestViewModelValidator(IServiceProvider serviceProvider)
        {
            
            //validation rules
            RuleFor(x => x.Name).NotNull().NotEmpty().MinimumLength(4)
                .MaximumLength(25).MustAsync(NameExists).WithMessage("Name already exists");
            RuleFor(x => x.Code).NotNull().NotEmpty().MinimumLength(3)
                .MaximumLength(10).MustAsync(CodeExists).WithMessage("Code already exists");
            RuleFor(x => x.Credit).NotNull().NotEmpty();

            _serviceProvider = serviceProvider;
        }

        //here, you can reference your custom validation
        //IsCodeExists is in ICourseService implememntation
        private async Task<bool> CodeExists(string code, CancellationToken arg2)
        {
            if (string.IsNullOrEmpty(code))
            {
                return true;
            }
            var requiredService = _serviceProvider.GetRequiredService<ICourseService>();

            return await requiredService.IsCodeExist(code);
        }

        private async Task<bool> NameExists(string name, CancellationToken arg2)
        {
            if (string.IsNullOrEmpty(name))
            {
                return true;
            }
            var requiredService = _serviceProvider.GetRequiredService<ICourseService>();

            return await requiredService.IsNameExist(name);
        }

        
    }
}
