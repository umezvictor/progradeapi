﻿using BLL.Services;
using FluentValidation;
using Microsoft.EntityFrameworkCore.Metadata;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Extensions.DependencyInjection;

namespace BLL.Request
{
    
    public class CourseAssignInsertViewModel
    {
        
        public int CourseId { get; set; }
        public int StudentId { get; set; }
        
    }

    //we did not use the usual annotation for validation
    public class CourseAssignInsertViewModelValidator : AbstractValidator<CourseAssignInsertViewModel>
    {
        private readonly IServiceProvider _serviceProvider;

        public CourseAssignInsertViewModelValidator(IServiceProvider serviceProvider)
        {

            //validation rules
            RuleFor(x => x.StudentId).NotNull().NotEmpty().
                MustAsync(StudentIdExists).WithMessage("Student Id does not exist");
            RuleFor(x => x.CourseId).NotNull().NotEmpty().
                MustAsync(CourseIdExists).WithMessage("Course Id does not exist"); ;
                //add more rules later
          

            _serviceProvider = serviceProvider;
        }


        private async Task<bool> StudentIdExists(int studentId, CancellationToken arg2)
        {
           
            var requiredService = _serviceProvider.GetRequiredService<IStudentService>();

            return ! await requiredService.IsIdExist(studentId);
        }

        private async Task<bool> CourseIdExists(int courseId, CancellationToken arg2)
        {
           
            var requiredService = _serviceProvider.GetRequiredService<ICourseService>();

            return ! await requiredService.IsIdExist(courseId);
        }

    }
}
