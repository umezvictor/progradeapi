﻿using BLL.Services;
using FluentValidation;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BLL.Request
{
    
    public class StudentInsertRequestViewModel
    {
        
        public string Name { get; set; }
        public string Email { get; set; }
        public int DepartmentId { get; set; }
    }

    public class StudentInsertRequestViewModelValidator : AbstractValidator<StudentInsertRequestViewModel>
    {
        private readonly IServiceProvider _serviceProvider;
        public StudentInsertRequestViewModelValidator(IServiceProvider serviceProvider)
        {
            
            //validation rules
            RuleFor(x => x.Name).NotEmpty().NotNull().MinimumLength(4).MaximumLength(50);
            RuleFor(x => x.Email).EmailAddress().NotNull().NotEmpty().MinimumLength(3).MustAsync(EmailExists)
                .WithMessage("Email already exists");
            RuleFor(x => x.DepartmentId).GreaterThan(0).MustAsync(DepartmentExists).WithMessage("department does not exists");

            _serviceProvider = serviceProvider;
        }



        private async Task<bool> EmailExists(string email, CancellationToken arg2)
        {
            if (string.IsNullOrEmpty(email))
            {
                return true;
            }
            var requiredService = _serviceProvider.GetRequiredService<IStudentService>();

            return await requiredService.IsEmailExist(email);
        }

        private async Task<bool> DepartmentExists(int id, CancellationToken arg2)
        {
            if (id == 0)
            {
                return true;
            }
            var requiredService = _serviceProvider.GetRequiredService<IStudentService>();

            return ! await requiredService.IsDepartmentExist(id);
        }
    }
}
